title:      "Reading 04: Software Installation"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The focus of this week's readings is **installing software**.  Since we have
  an exam this week, the readings will serve as references.  For the most part,
  you simply need to lookup or search for the answers to the questions below
  and make the summary pages.

  1. **Archives**:

      a. [An Introduction to File Compression Tools on Linux Servers](https://www.digitalocean.com/community/tutorials/an-introduction-to-file-compression-tools-on-linux-servers)

      b. [How to Zip and Unzip in Linux?](http://robdvr.com/zip-unzip-linux/)

  2. **Package Managers**:

      a. [Yum Command Cheat Sheet for Red Hat Enterprise Linux](https://access.redhat.com/articles/yum-cheat-sheet)

      b. [apt-get command cheat sheet for Debian Linux](http://www.cyberciti.biz/howto/question/linux/apt-get-cheat-sheet.php)

      c. [Linux Commands For Beginners: SUDO](https://linuxacademy.com/blog/linux/linux-commands-for-beginners-sudo/)

  3. **Custom Software**:

      a. [The magic behind configure, make, make install](https://robots.thoughtbot.com/the-magic-behind-configure-make-make-install)

  Also, please take a look at the manual pages linked below.

  ## Questions

  In your `reading04/README.md` file, describe what command you would use to
  accomplish the following:

  1. Extract the contents of `file.tar.gz`.

  2. Create an archive named `data.tar.gz` which consists of the contents of
  the directory `data`.

  3. Extract the contents of `file.zip`.

  4. Create an archive named `data.zip` which consists of the contents of the
  directory `data`.

  5. Install a package on a [Debian] based distribution.

  6. Install a package on a [Red Hat] based distribution.

  7. Install a [Python] package.

  8. Paste the contents of a file to an online [pastebin].

  9. Run commands as the [root] user.

  [Debian]:     https://www.debian.org/
  [Red Hat]:    http://www.redhat.com/
  [Python]:     https://www.python.org/
  [pastebin]:   https://en.wikipedia.org/wiki/Pastebin
  [root]:       http://www.linfo.org/root.html

  ## Commands

  In the `reading04` folder, write at least **five** summary pages for the
  following commands:

  1. [tar](http://man7.org/linux/man-pages/man1/tar.1.html)

  2. [gzip](http://linux.die.net/man/1/gzip)

  3. [zip](http://linux.die.net/man/1/zip)

  4. [unzip](http://linux.die.net/man/1/unzip)

  5. [yum](http://man7.org/linux/man-pages/man8/yum.8.html)

  6. [apt-get](http://linux.die.net/man/8/apt-get)

  7. [sudo](https://www.sudo.ws/man/sudo.man.html)

  8. [su](http://man7.org/linux/man-pages/man1/su.1.html)

  9. [pip](https://pip.pypa.io/en/stable/quickstart/)

  10. [sprunge](http://sprunge.us/) / [yld.me](http://yld.me/)

      **Note**: these are services, rather than specific commands, although you
        can make [aliases] for them.

  [aliases]:    https://en.wikipedia.org/wiki/Alias_(command)#C_shell

  <div class="alert alert-warning" markdown="1">
  #### <i class="fa fa-warning"></i> Summaries

  Note, your summaries should be in your own words and not simply copy and
  pasted from the manual pages.  They should be short and **concise** and only
  include **common** use cases.

  </div>

  ## Guru Point (1 Point)

  For extra credit, download and install the [CMatrix] program to your a
  location in your `$HOME` directory.  Additionally, adjust your `$PATH` so you
  can execute the program by simply typing `cmatrix`.

  To get credit, you must show either a TA or the instructor a demonstration of
  [CMatrix] doing its thing.

  [CMatrix]:  http://www.asty.org/cmatrix/

  ## Feedback

  If you have any questions, comments, or concerns regarding the course, please
  provide your feedback at the end of your response.

  ## Submission

  To submit your assignment, please commit your work to the `reading04` folder
  in your **Assignments** [Bitbucket] repository by **the beginning of class**
  on **Monday, February 8**.

  [Bitbucket]:    https://bitbucket.org
