title:      Checklist 01
icon:       fa-check-square-o
navigation: []
internal:   []
external:   []
body:       |

    Here is a general outline of the **key** concepts and commands (arranged by
    topic) that you should know for **Exam 01**.
    
    ## Introduction

    ### Concepts

    * What is **Unix**?

    * What is a **shell**?

    * How would do you use **git** to:

        - Create a copy a repository.

        - View the changes you've made.

        - Save a change you've made.

        - Share the changes you'ved made.

        - Retrieve the changes from a remote location.

    * What are **environment variables**?  What does the `PATH` environment
    variable control?  How would you modify it?

    ### Commands

    1. [git](http://man7.org/linux/man-pages/man1/git.1.html)

        Version control system for managing our files.
    
    ## Files 

    ### Concepts

    * What is a **file**?

    * What is the general layout or hierarchy of a typical Unix
    **filesystem**?

    * What is an **inode** and what information does it store?

    * What is the difference between an **absolute** and **relative** path?

    * How do Unix file **permissions** work?  How would you set them to
    restrict access for certain operations and certain classes?

        You should be able to translate from **octal** to `rwx` triplets and
        back.

    * What is the difference between a **hard link** and a **soft link**?
    
    ### Commands

    1. [ls](http://man7.org/linux/man-pages/man1/ls.1.html)

        List the contents of a directory (long, human-readable, sorted)

    2. [chmod](http://man7.org/linux/man-pages/man1/chmod.1.html)

        Set permissions using **octal** and symbolic arguments.

    3. [stat](http://man7.org/linux/man-pages/man1/stat.1.html)

        View the inode information fora file.

    4. [du](http://man7.org/linux/man-pages/man1/du.1.html)

        View the total disk usage for a file or directory.

    5. [ln](http://man7.org/linux/man-pages/man1/ln.1.html)

        Create hard and soft links.

    6. [find](http://man7.org/linux/man-pages/man1/find.1.html)

        Search directory for files by name and type.
    
    ## Process
    
    ### Concepts

    * What is a **process**?  What attributes does it have?

    * What does it mean to **signal** a process?  What do different the
    different signals do (`TERM`, `INT`, `KILL`, `HUP`)?

    * How do we **suspend**, **foreground**, and **background** a process?

    ### Commands

    1. [ps](http://man7.org/linux/man-pages/man1/ps.1.html)

        List the processes for the current user and for the whole system.

    2. [top](http://man7.org/linux/man-pages/man1/top.1.html)

        Interactively view and manage processes.

    3. [kill](http://man7.org/linux/man-pages/man1/kill.1.html)

        Signal a process by PID.

    4. [time](http://man7.org/linux/man-pages/man1/time.1.html)

        Measure how long it takes a process to execute.
    
    5. [pkill](http://man7.org/linux/man-pages/man1/kill.1.html)

        Signal a process by name.

    ## I/O Redirection
    
    ### Concepts

    * What three **files** does every process start with?

    * How do you redirect the output of a command into a new file?

    * How do you redirect the output of a command and append it to a file?

    * How do you redirect the output of one command as the input to another?

    * How do you direct the contents of a file into the input of command?

    * What is `/dev/null`?  How would you use it to output `stderr` but ignore
    `stdout` of a program?

    * What is a **here document** and how would you use it?
    
    ### Commands
    
    1. [tee](http://man7.org/linux/man-pages/man1/tee.1.html)

        Redirect `stdin` to both `stdout` and a file.

    2. [write](http://man7.org/linux/man-pages/man1/write.1.html)

        Write to another users terminal.
    
    ## Networking
    
    ### Concepts
    
    * What is **bandwidth** and **latency**?  How are they different?  How
    would we measure both of them?

    * What is an **IP address**?  How is a **domain name** related to it?
    
    * What is a network **port**?

    * What is the basic difference between **TCP** and **UDP**?

    * How does **HTTP** work?  How would you manually perform a **HTTP**
    request?
    
    ### Commands
    
    1. [scp](http://man7.org/linux/man-pages/man1/scp.1.html) /
    [sftp](http://man7.org/linux/man-pages/man1/sftp.1.html) /
    [rsync](http://linux.die.net/man/1/rsync)

        Transfer a file from one machine to another.

    2. [ip](http://man7.org/linux/man-pages/man8/ip.8.html) / [ifconfig](http://man7.org/linux/man-pages/man8/ifconfig.8.html)

        List the IP addresses of the current machine.

    3. [tmux](http://man7.org/linux/man-pages/man1/tmux.1.html) / [screen](http://man7.org/linux/man-pages/man1/screen.1.html)

        Maintain a persistent shell session.

    4. [nc](http://linux.die.net/man/1/nc) / [telnet](http://linux.die.net/man/1/telnet)

        General purpose tools to communicate with TCP-based network services.

    5. [wget](http://man7.org/linux/man-pages/man1/wget.1.html) / [curl](http://curl.haxx.se/docs/manpage.html)

        Communicate using HTTP (ie. download files from the web).

    ## Developer Tools
    
    ### Concepts

    * What exactly happens when you **compile** a program (ie. describe the
    **compiler pipeline**)?  What are do the compiler flags `-std=c99`,
    `-Wall`, `-g`, and `-O2` do?

    * What is the difference between a **dynamic** executable and a **static**
    executable?
    
    * What is a **shared** library?  How does the environment variable
    `LD_LIBRARY_PATH` affect **shared libraries**?  How do we list the **shared
    libraries** an executable requires?

    * How does **make** work?

        You should be able to write `Makefiles` that utilize **rules**,
        **pattern rules**, **variables**, and **magic variables**.

    * What are some different ways to **debug** an application?  How would you
    track down a **memory** error?

    ### Commands
    
    1. [gcc](http://man7.org/linux/man-pages/man1/gcc.1.html)

    2. [ldd](http://man7.org/linux/man-pages/man1/ldd.1.html)

    3. [make](http://man7.org/linux/man-pages/man1/make.1.html)

    4. [valgrind](http://man7.org/linux/man-pages/man1/valgrind.1.html)

    5. [gdb](http://man7.org/linux/man-pages/man1/gdb.1.html)
