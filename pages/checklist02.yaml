title:      Checklist 02
icon:       fa-check-square-o
navigation: []
internal:   []
external:   []
body:       |

    Here is a general outline of the **key** concepts and commands (arranged by
    topic) that you should know for **Exam 02**.

    The exam will have the following format:

    1. **Commands**: Identify which command performs a certain task.

    2. **Short Answers**: Briefly answer questions about the **Unix
      Philosophy** and **Software Installation**.

    3. **Debugging**: Fix some simple shell scripts.

    4. **Code Evaluation**: Determine the results of commands involving `cut`,
      `tr`, `grep`, or `sed`.

    5. **Filters**: Use Unix **filters** to answer some questions.

    6. **Scripting**: Write a shell script that utilizes command line arguments
      and filters.

    Parts **5** and **6** can be done with the aid of your laptop and the
    Internet (but not other people).  Parts **1** through **4** is to be done
    first on paper.

    ## Software Installation

    ### Concepts

    1. What is the purpose of an **installer**?  What sort of things does this
    type of program do (identify at least **three** things)?

    2. What is a **package manager** and why do many Unix systems use them?

    3. When do you need **administrator priviledges** to install software?

    4. How would you use `configure` and `make` to install software to your
    `$HOME` directory?  What **environment variables** would you need to update
    to ensure you can execute the program you installed?  Use the libraries?

    ### Commands

    1. [tar](http://man7.org/linux/man-pages/man1/tar.1.html)

    2. [unzip](http://linux.die.net/man/1/unzip)

    3. [sudo](https://www.sudo.ws/man/sudo.man.html)

    ## Shell Scripting

    ### Concepts

    1. What is a **shell script**?  What is a **she-bang** and why do we need
    it (or what does it do)?

    2. You should know the basics of the shell programming language:

        1. **variables**

        2. **capturing STDOUT**

        3. **if** statement

        4. **case** statement

        5. **for** loop

        6. **while** loop

        7. **function**

        8. **trap**

    3. How do you **debug** a shell script?  What different modes do we have to
    help us figure out what a script is doing?

    ### Commands

    1. [sh](https://www.freebsd.org/cgi/man.cgi?query=sh&sektion=&n=1)

    2. [test](http://man7.org/linux/man-pages/man1/test.1.html)

    ### Sample Questions

    #### Debugging

    The following scripts contain errors. Identify and correct all the bugs.

    1.  This script checks each file passed via command line arguments and
    reports if the file is readable.

            :::text
            for $file in $@; do
                [-r $file] | echo file is readable!
            done

    2.  This script checks each file passed via command line arguments and
    reports if the file is readable.

            :::text
            is_readable() {
                test -r $@
            }

            while ( -n "$@" ); do
                is_readable($@) | echo file is readable!
            done

    #### Scripting

    Write the following shell script:

        :::text
        $ ./column.sh -h
        usage: column.sh [-n N -d D -S -R] column

            -n N    Number of items to display (default is 5).
            -d D    Delimiter to use (default is ",").
            -S      Sort output.
            -R      Randomize output.

        This script selects a column (default is 1) from the CSV input and then
        displays the first N items.

    **Examples**:

        ::text
        $ ./column.sh -d : < tmnt.txt
        Leonardo
        Donatello
        Raphael
        Michelangelo

        $ ./column.sh -d : -n 1 < tmnt.txt
        Leonardo

        $ ./column.sh -d : -n 1 3 < tmnt.txt
        katana

        $ ./column.sh -d : -S 3 < tmnt.txt
        bo
        katana
        nunchucks
        sai

        $ ./column.sh -d : -R 3 < tmnt.txt
        nunchucks
        katana
        sai
        bo

    **Note**:  The `tmnt.txt` file is described below.

    ## Filters

    ### Concepts

    1. What are the **three tenets** of the **Unix Philosophy**?

    2. What are **regular expressions**?

        You will need to know the basic **metacharacters** such as

        - `.`
        - `*`
        - `[]`
        - `[^]`
        - `^`
        - `$`
        - `()`
        - `|`
        - `\n`
        - `{m, n}`

        You will also need to be familiar with the basic **character classes**
        and their **character set** equivalents such as:

        - `[:alpha:] = [a-zA-Z]`
        - `[:lower:] = [a-z]`
        - `[:upper:] = [A-Z]`
        - `[:space:] = [ \t\r\n\v\f]`
        - `[:digit:] = [0-9]`

    3. What are **filters**?  What do they do and how are they related to the
    **Unix Philosophy**?

    ### Commands

    1. [cut](http://man7.org/linux/man-pages/man1/cut.1.html)

    2. [sort](http://man7.org/linux/man-pages/man1/sort.1.html)

    3. [uniq](http://man7.org/linux/man-pages/man1/uniq.1.html)

    4. [shuf](http://man7.org/linux/man-pages/man1/shuf.1.html)

    5. [tr](http://man7.org/linux/man-pages/man1/tr.1.html)

    6. [grep](http://man7.org/linux/man-pages/man1/grep.1.html)

    7. [sed](http://man7.org/linux/man-pages/man1/sed.1.html)

    8. [head](http://man7.org/linux/man-pages/man1/head.1.html)

    9. [tail](http://man7.org/linux/man-pages/man1/tail.1.html)

    10. [awk](http://man7.org/linux/man-pages/man1/awk.1.html)

    ### Sample Questions

    Given the file `tmnt.txt`, which contains:

        :::text
        Leonardo:blue:katana
        Donatello:purple:bo
        Raphael:red:sai
        Michelangelo:orange:nunchucks

    #### Code Evaluation

    What is the result of the following shell commands?

    1. **cut**:

        1. `echo "Leonardo,Donatello,Raphael,Michelangelo" | cut -d , -f 1`

        2. `echo "Leonardo,Donatello,Raphael,Michelangelo" | cut -d , -f 1,3`

        3. `echo "Leonardo,Donatello,Raphael,Michelangelo" | cut -d , -f 2-`

        4. `echo "Leonardo,Donatello,Raphael,Michelangelo" | cut -b 1,15,39`

    2. **tr**:

        1. `echo "Pizza! Pizza!" | tr '!' '?'`

        2. `echo "Pizza! Pizza!" | tr 'iaz' '145'`

        3. `echo "Pizza! Pizza!" | tr -d '[n-z]'`

        4. `echo "Pizza! Pizza!" | tr -d '[:punct:]'`

    3. **grep**:

        1. `cat tmnt.txt | grep '^[LM]'`

        2. `cat tmnt.txt | grep 'e.*o'`

        3. `cat tmnt.txt | grep -Eo '^[^: ]+'`

        4. `cat tmnt.txt | grep -Eo '[a-zA-Z]+[al]{2}[^:]*'`

    4. **sed**:

        1. `cat tmnt.txt | sed -nr 's/Leonardo/Splinter/p'`

        2. `cat tmnt.txt | sed -r '/lo/d'`

        3. `cat tmnt.txt | sed -nr 's|.*:o([^: ]+).*|O\1|p'`

        4. `cat tmnt.txt | sed -nr 's/.*([LD][^:]+)[: ]+([^:]+).*/\2/p'`

    #### Filters

    Given the `tmnt.txt` file above, write Unix pipelines that perform the
    following tasks:

    - List only the names of the turtles in sorted order.

    - List only the colors of the turtles in sorted order.

    - List only the weapons of the turtles in sorted order.

    - List only the turtles whose names in `lo`.

    - List only the turtles with last names with two consecutive vowels.

    - Count how many colors begin with a `b`.

    - Count how many colors end with an `e`.

    - List only the turtles names whose name ends with a vowel and whose weapon
      ends with a vowel.

    - List only the turtles names with two of the same consecutive letter (i.e.
      `aa`, `bb`, etc.)

    - List the colors that don't end with a vowel.

    - List the weapons that contains a repeat letter anywhere in the string.
